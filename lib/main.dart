import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:screenshot/screenshot.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  GoogleMapController mapController;
  File _imageFile;

  //Create an instance of ScreenshotController
  ScreenshotController screenshotController = ScreenshotController();

  final LatLng _center = const LatLng(45.521563, -122.677433);

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Maps Sample App'),
          backgroundColor: Colors.green[700],
        ),
        body: Stack(
          children: <Widget>[
            Screenshot(
              controller: screenshotController,
              child: GoogleMap(
                onMapCreated: _onMapCreated,
                initialCameraPosition: CameraPosition(
                  target: _center,
                  zoom: 11.0,
                ),
              ),
            ),
            if (_imageFile != null)
              Container(
                color: Colors.red,
                child: Image(
                  fit: BoxFit.contain,
                  image: FileImage(
                    _imageFile,
                  ),
                ),
                width: 500,
                height: 500,
              ),
            Positioned(
              child: RaisedButton(
                onPressed: () {
                  screenshotController.capture().then((File image) async {
                    _imageFile = image;
                    setState(() {});
                  }).catchError((onError) {
                    print(onError);
                  });
                },
                child: Text('capture screen'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
